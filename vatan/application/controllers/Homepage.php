<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

    function __construct()
    {
        parent:: __construct();
        $this->result = new StdClass();
        $this->result->status = false;
        $this->load->model($this->router->fetch_class() . '_model', 'model');

    }

    public function response()
    {
        echo json_encode($this->result);

    }

	public function index()
	{
		$this->load->view('index');
	}


}
