<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

    function __construct()
    {
        parent:: __construct();
        $this->result = new StdClass();
        $this->result->status = false;
        $this->load->model($this->router->fetch_class() . '_model', 'model');

    }

    public function response()
    {
        echo json_encode($this->result);

    }

    public function add_kayit()
    {
        $post_data = new StdClass();
        $this->form_validation->set_rules('name_surname', 'Ad Soyad', 'required|trim');
        $this->form_validation->set_rules('email', 'Mail Adresi', 'required|trim|valid_email|xss_clean');
        $this->form_validation->set_rules('username', 'Kullanıcı Adı', 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', 'Şifre', 'required|trim|xss_clean');



        if ($this->form_validation->run() != false) {
            $post_data->name_surname = $this->input->post('name_surname', true);
            $post_data->email = $this->input->post('email', true);
            $post_data->username = $this->input->post('username', true);
            $post_data->password = $this->input->post('password', true);

            $new_kayit = $this->model->get_kayit_mail($post_data->email);


            if (empty($new_kayit)) {
                if ($this->model->add_kayit($post_data)) {
                    $this->result->status = true;
                } else {
                    $this->result->error = "Bir Sorun Oluştu Lütfen Tekrar Deneyin";
                }
            } else {
                $this->result->error = "Bu E-posta adresine kayıtlı üye bulunmaktadır.";
            }



        } else {
           // prex('hahaaaaaaaaa');
            $this->result->error = validation_errors();
        }
        $this->response();
    }




}