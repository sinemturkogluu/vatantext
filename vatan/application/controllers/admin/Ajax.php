<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Ajax extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->result = new StdClass();
        $this->result->status = false;
        $this->load->model('admin/' . $this->router->fetch_class() . '_model', 'model');
    }

    public function response()
    {
        echo json_encode($this->result);
    }


    public function delete_users()
    {
        $id = $this->input->post('id', true);

        if ($this->model->delete_user($id)) {
            $this->result->status = true;
        } else {
            $this->result->error = "İşlem Başarısız Tekrar Deneyin.";
        }
        $this->response();
    }

     public function add_blog()
     {
         $post_data = new StdClass();
         $this->form_validation->set_rules('name', 'Başlık', 'required|trim|xss_clean');
         $this->form_validation->set_rules('text', 'Kapak Metni', 'required|trim|xss_clean');
         $this->form_validation->set_rules('description', 'Blog Açıklaması', 'required|trim|xss_clean');

         if ($this->form_validation->run() != false) {
             $post_data->name = $this->input->post('name', true);
             $post_data->text = $this->input->post('text', true);
             $post_data->description = $this->input->post('description', true);
             $post_data->user_id = $this->session->userdata('admin_info')->id;

             if ($this->session->userdata('images')) {

                 $post_data->image = $this->session->userdata('images');
                 $post_data->image = $post_data->image[0];
                 $this->session->unset_userdata('images');


                 if ($this->model->add_blog($post_data)) {
                     $this->result->status = true;
                 } else {
                     $this->result->error = "Bir Sorun Oluştu Lütfen Tekrar Deneyin";
                 }
             } else {
                 $this->result->error = "Lütfen Resim Ekleyiniz.";
             }


         } else {
             $this->result->error = validation_errors();
         }
         $this->response();
     }

     public function status_blog()
     {
         $id = $this->input->post('id', true);
         $status = $this->input->post('status', true);

         if ($this->model->status_update($id, $status)) {
             $this->result->status = true;
         } else {
             $this->result->error = "İşlem Başarısız Tekrar Deneyin.";
         }
        $this->response();
     }

       public function update_blog()
       {
           $post_data = new StdClass();

           $this->form_validation->set_rules('id', 'Id', 'trim|xss_clean');
           $this->form_validation->set_rules('name', 'Başlık', 'trim|xss_clean');
           $this->form_validation->set_rules('text', 'Kapak Metni', 'trim|xss_clean');
           $this->form_validation->set_rules('description', 'Blog Açıklaması', 'trim|xss_clean');

           if ($this->form_validation->run() != false) {
               $post_data->id = $this->input->post('id', true);
               $post_data->name = $this->input->post('name', true);
               $post_data->text = $this->input->post('text', true);
               $post_data->description = $this->input->post('description', true);
               $post_data->user_id = $this->session->userdata('admin_info')->id;

               if ($this->session->userdata('images')) {

                   $post_data->image = $this->session->userdata('images');
                   $post_data->image = $post_data->image[0];
                   $this->session->unset_userdata('images');
               }

               if ($this->model->update_blog($post_data)) {
                   $this->result->status = true;
               } else {
                   $this->result->error = "Güncelleme Esnasında Bir Hata Oluştu Lütfen Tekrar Deneyin";
               }

           } else {
               $this->result->error = validation_errors();
           }
           $this->response();
       }

       public function delete_blog()
       {
           $id = $this->input->post('id', true);

           if ($this->model->delete_blog($id)) {
               $this->result->status = true;
           } else {
               $this->result->error = "İşlem Başarısız Tekrar Deneyin.";
           }
           $this->response();
       }

}