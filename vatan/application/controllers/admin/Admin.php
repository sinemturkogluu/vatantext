<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent:: __construct();

		$this->load->model('admin/'.$this->router->fetch_class() . '_model', 'model');
	}

	public function index()
	{
		if (!empty($this->session->userdata('oturum'))) {
            redirect(site_url('Admin/uey-listesi'));
        }
        $data = new stdClass();
        $data->csrf=['name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()];
        $post_data = new stdClass();

        $this->form_validation->set_rules('username', 'Kullanıcı Adı', 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', 'Şifre', 'required|trim|xss_clean');

        if ($this->form_validation->run() != false) {
            $post_data->username = $this->input->post('username', true);
            $post_data->password = $this->input->post('password', true);

            if ($this->model->login($post_data)) {
               // redirect(base_url('admin/anasayfa'));
                site_url('admin');
            } else {
                $data->hata = "Kullanıcı Adı Veya Şifre Hatalı";
            }
        }
        if (empty($this->session->userdata('admin_info'))) {

            $this->load->view('index');
        }else{
            redirect(site_url('admin/anasayfa'));
        }
	}

    public function homepage_admin()
    {
        if (empty($this->session->userdata('admin_info'))) {
            redirect(site_url('admin/anasayfa'));
        }

        $this->load->view('admin/header');
        $this->load->view('admin/index');
        $this->load->view('admin/footer');
    }


    public function users_list()
    {
        if (empty($this->session->userdata('admin_info'))) {
            redirect(site_url('admin/anasayfa'));
        }

        $data = new stdClass();
        $data->list = $this->model->get_users();

        $this->load->view('admin/header', $data);
        $this->load->view('admin/index');
        $this->load->view('admin/footer');
    }

    public function blog_add()
    {
        if (empty($this->session->userdata('admin_info'))) {
            redirect(site_url('admin/anasayfa'));
        }

        $data = new stdClass();


        $this->load->view('admin/header', $data);
        $this->load->view('admin/blog/add');
        $this->load->view('admin/footer');
    }

    public function blog_list()
    {
        if (empty($this->session->userdata('admin_info'))) {
            redirect(site_url('admin/anasayfa'));
        }

        $data = new stdClass();

        $data->blog = $this->model->get_blog();

        $this->load->view('admin/header', $data);
        $this->load->view('admin/blog/list');
        $this->load->view('admin/footer');
    }

    public function blog_update($id)
    {
        if (empty($this->session->userdata('admin_info'))) {
            redirect(site_url('admin/anasayfa'));
        }

        $data = new stdClass();

        $data->blog = $this->model->get_selected_blog($id);
//prex($data->blog);
        $this->load->view('admin/header', $data);
        $this->load->view('admin/blog/update');
        $this->load->view('admin/footer');
    }


}