<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_model extends CI_MODEL {



	public function add_kayit($post_data)
    {
        $this->db->set($post_data)->insert('users');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function get_kayit_mail($email)
    {
        $this->db->from('users');
        $this->db->where(['email' => $email]);
        $return_query = $this->db->get();
        if($return_query->num_rows() > 0) {
            return $return_query->row();
        } else {
            return false;
        }
    }



}