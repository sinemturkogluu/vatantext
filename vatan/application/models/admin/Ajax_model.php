<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ajax_model extends CI_Model
{

	public function delete_user($id)
    {
        $this->db->where(['id' => $id])->delete('users');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function add_blog($post_data)
    {
        $this->db->set($post_data)->insert('blog');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function status_update($id, $status)
    {
        $this->db->set(['status' => $status])->where(['id' => $id])->update('blog');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function update_blog($post_data)
    {
        $this->db->set($post_data)->where(['id' => $post_data->id])->update('blog');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_blog($id)
    {
        $this->db->set(['deleted' =>1 ])->where(['id' =>$id])->update('blog');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


}