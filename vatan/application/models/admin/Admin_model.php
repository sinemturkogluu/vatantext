<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public function login($post_data)
	{
		$this->db->from('users');
		$this->db->where(['username' => $post_data->username]);
		$return_query = $this->db->get();
		if ($return_query->num_rows() > 0) {
			$admininfo = $return_query->row();
           // prex($admininfo);
			if ($admininfo->password == $post_data->password) {
				$this->session->set_userdata('admin_info', $admininfo);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

    public function get_users()
    {
        $this->db->from('users');
        $return_query = $this->db->get();
        if($return_query->num_rows() > 0) {
            return $return_query->result();
        } else {
            return false;
        }
    }

    public function get_blog()
    {
        $this->db->select('b.*, b.id as b_id, b.name as b_name, b.status as b_status, b.text as b_text, b.description as b_description,
                                    b.image as b_image, b.deleted as b_deleted,
    							   u.*, u.id as u_id, u.name_surname as u_name_surname,u.username as u_username');
        $this->db->from('blog b');
        $this->db->join('users u', 'b.user_id = u.id');
        $this->db->where(['deleted' => 0]);
        $return_query = $this->db->get();
        if($return_query->num_rows() > 0) {
            return $return_query->result();
        } else {
            return false;
        }
    }

    public function get_selected_blog($id)
    {
        $this->db->from('blog');
        $this->db->where(['id' => $id , 'deleted' =>0]);
        $return_query = $this->db->get();
        if($return_query->num_rows() > 0) {
            return $return_query->row();
        } else {
            return false;
        }
    }








}