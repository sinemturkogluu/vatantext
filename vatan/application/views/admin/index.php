<div class="content-wrapper">
	<div class="page-title">
		<div class="row">
			<div class="col-sm-6">
			    <h4 class="mb-0"> Admin panel</h4>
			</div>
			<div class="col-sm-6">
			    <ol class="breadcrumb pt-0 pr-0 float-left float-sm-right">
			    	<li class="breadcrumb-item"><a href="<?php echo site_url('admin') ?>" class="default-color">Anasayfa</a></li>

			    </ol>
			</div>
		</div>
	</div>

	<div class="global-space">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                    <tr>
                                        <th style="width: 10%;">#</th>
                                        <th style="width: 10%;">Adı Soyadı</th>
                                        <th style="width: 20%;">Kullanıcı Adı</th>
                                        <th style="width: 20%;">E-Posta</th>
                                        <th style="width: 10%;">Sil</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(!empty($list)) { ?>
                                        <?php foreach ($list as $key => $val) { ?>
                                            <tr id="delete<?php echo $val->id; ?>">
                                                <td><?php echo $val->id; ?></td>

                                                <td><?php echo $val->name_surname; ?></td>

                                                <td><?php echo $val->username; ?></td>
                                                <td><?php echo $val->email; ?></td>
                                                <td>
                                                    <button class="w-100 btn btn-danger buttonAnimation delete-users"
                                                            data-id="<?php echo $val->id; ?>"
                                                            data-animation="bounce">Sil<i class=" la la-trash"></i></button>
                                                </td>
                                            </tr>

                                        <?php } ?>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>

