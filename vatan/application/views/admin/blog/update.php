<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">Blog</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo site_url('yonetim-paneli'); ?>">Anasayfa</a>
                            </li>

                            <li class="breadcrumb-item active">Blog Ekle</li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">

                                <form class="form" id="infoUpdateBlog" method="post">

                                    <input type="text" hidden  class="form-control"  name="id" value="<?php echo $blog->id; ?>">

                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                <label>Başlık</label>
                                                <input type="text"  class="form-control" value="<?php echo !empty($blog->name) ? $blog->name : '' ?>" name="name" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <label>Kapak Metni</label>
                                            <textarea id="text" class="form-control" name="text"><?php echo !empty($blog->text) ? $blog->text : '' ?></textarea>
                                        </div>
                                    </div>

                                    <div class="row mt-2">
                                        <div class=" col-12 col-md-12">
                                            <p class="add_image_text">Blog Resmi Ekle</p>
                                            <div class="form-group">
                                                <div action="<?php echo site_url('admin/Add_images/add-image'); ?>" class="dropzone dropzone-area" id="dpz-single-file">
                                                    <div class="dz-message">
						                                    <span class="m-dropzone__msg-desc">
						                                        Bir Adet Resim Seçiniz.
						                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-2">
                                        <div class="col-12 col-md-12">
                                            <label>Blog Açıklaması</label>
                                            <textarea id="text" class="ckeditor" name="description"><?php echo !empty($blog->description) ? $blog->description : '' ?></textarea>
                                        </div>
                                    </div>


                                    <div class="form-actions right">
                                        <button type="submit" class="btn btn-outline-success">
                                            <i class="ft-check"></i> Kaydet
                                        </button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>