<div class="content-wrapper">
    <div class="page-title">
        <div class="row">
            <div class="col-sm-6">
                <h4 class="mb-0">Admin panel</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb pt-0 pr-0 float-left float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo site_url('admin') ?>" class="default-color">Anasayfa</a></li>
                    <li class="breadcrumb-item">Blog Listele</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="global-space">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                    <tr>
                                        <th style="width: 10%;">Ekleyen Kullanıcı</th>
                                        <th style="width: 30%;">Başlık</th>
                                        <th style="width: 10%;">Detaylar</th>
                                        <th style="width: 10%;">Göster</th>
                                        <th style="width: 10%;">Düzenle</th>
                                        <th style="width: 10%;">Sil</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(!empty($blog)) { ?>
                                        <?php foreach ($blog as $key => $val) { ?>
                                            <tr id="delete<?php echo $val->b_id; ?>">
                                                <td><?php echo $val->u_name_surname; ?></td>
                                                <td><?php echo $val->b_name; ?></td>


                                                <td>
                                                    <button type="button" class="btn btn-outline-primary block" data-toggle="modal" data-target="#default<?php echo $val->b_id; ?>">
                                                        Detay
                                                    </button>
                                                </td>

                                                <td>
                                                    <div style="text-align: center">
                                                        <input  type="checkbox" class="blog-status"
                                                                data-id="<?php echo $val->b_id; ?>"
                                                                id="switch1" <?php echo $val->b_status == 1 ? "checked " :""; ?> />
                                                    </div>
                                                </td>

                                                <td>
                                                    <button onclick="window.location.href='<?php echo site_url('admin/blog-duzenle/'.$val->b_id); ?>'" class="w-100 btn btn-warning buttonAnimation" data-animation="pulse">Düzenle<i class="la la-refresh"></i></button>
                                                </td>
                                                <td>
                                                    <button class="w-100 btn btn-danger buttonAnimation delete-blog"
                                                            data-id="<?php echo $val->b_id; ?>"
                                                            data-animation="bounce">Sil<i class=" la la-trash"></i></button>
                                                </td>
                                            </tr>
                                            <div class="modal fade text-left" id="default<?php echo $val->b_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                <div class=" modal-lg modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div style="text-align: center">
                                                                <?php echo $val->b_name; ?>
                                                            </div>

                                                            <?php echo $val->b_text; ?>

                                                            <p><img style="height: 130px " src="<?php echo site_url('assets_admin/uploads/'.$val->b_image) ; ?>"></p>

                                                            <p><?php echo $val->b_text; ?></p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Kapat</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php } ?>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

