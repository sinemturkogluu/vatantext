<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Webmin - Bootstrap 4 & Angular 5 Admin Dashboard Template" />
<meta name="author" content="potenzaglobalsolutions.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Admin Panel Giriş</title>


<link rel="shortcut icon" href="<?php echo site_url('assets_admin/images/logo-icon-dark.png'); ?>" />
<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets_admin/css/style.css'); ?>" />
 
</head>

	<body>

		<div class="wrapper">
			<div id="pre-loader">
			    <img src="<?php echo site_url('assets_admin/images/pre-loader/loader-01.svg'); ?>" alt="">
			</div>

			<section class="height-100vh d-flex align-items-center page-section-ptb login" style="background-color: #E4E4E4;">
				<div class="container">
					<div class="row justify-content-center no-gutters vertical-align text-center">
						<div  class="col-lg-4 col-md-6 login-fancy-bg bg-light">
							<h1 class="mt-5">Admin Panel</h1>
						</div>
						<div class="col-lg-4 col-md-6 bg-white">
							<div class="login-fancy pb-40 clearfix">
								<h3 class="mb-30">Yönetici Girişi</h3>
								<form action="<?php echo site_url('admin/Admin');?>" method="post">

									<input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />

                                    <div class="section-field mb-20">
										<label class="mb-10" for="name">Kullanıcı Adı </label>
										<input id="name" class="web form-control" type="text" name="username">
										<?php if (form_error('username')) { ?>
											<div class="h-100 m-auto p-3 alert alert-danger" role="alert">
										<?php echo form_error('username'); ?>
											</div>
										<?php } ?>
									</div>
									<div class="section-field mb-20">
										<label class="mb-10" for="Password">Şifre </label>
										<input id="Password" class="Password form-control" type="password" name="password">
										<?php if (form_error('password')) { ?>
											<div class="h-100 m-auto p-3 alert alert-danger" role="alert">
										<?php echo form_error('password'); ?>
											</div>
										<?php } ?>
										<?php if (isset($hata)) { ?>
											<div class="m-auto p-3 alert alert-danger" role="alert">
										<?php echo $hata; ?>
											</div>
										<?php } ?>
									</div>
									<button class="button" >Giriş Yap <i class="fa fa-check"></i></button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section> 
		</div>

		<script src="<?php echo site_url('assets_admin/js/jquery-3.3.1.min.js'); ?>"></script>

		<!-- plugins-jquery -->
		<script src="<?php echo site_url('assets_admin/js/plugins-jquery.js'); ?>"></script>

		<!-- plugin_path -->
		<script>var plugin_path = '<?php echo site_url('assets_admin/js/'); ?>';</script>

		<!-- chart -->
		<script src="<?php echo site_url('assets_admin/js/chart-init.js'); ?>"></script>

		<!-- calendar -->
		<script src="<?php echo site_url('assets_admin/js/calendar.init.js'); ?>"></script>

		<!-- charts sparkline -->
		<script src="<?php echo site_url('assets_admin/js/sparkline.init.js'); ?>"></script>

		<!-- charts morris -->
		<script src="<?php echo site_url('assets_admin/js/morris.init.js'); ?>"></script>

		<!-- datepicker -->
		<script src="<?php echo site_url('assets_admin/js/datepicker.js'); ?>"></script>

		<!-- sweetalert2 -->
		<script src="<?php echo site_url('assets_admin/js/sweetalert2.js'); ?>"></script>

		<!-- toastr -->
		<script src="<?php echo site_url('assets_admin/js/toastr.js'); ?>"></script>

		<!-- validation -->
		<script src="<?php echo site_url('assets_admin/js/validation.js'); ?>"></script>

		<!-- lobilist -->
		<script src="<?php echo site_url('assets_admin/js/lobilist.js'); ?>"></script>
		 
		<!-- custom -->
		<script src="<?php echo site_url('assets_admin/js/custom.js'); ?>"></script>
	 
	</body>
</html>