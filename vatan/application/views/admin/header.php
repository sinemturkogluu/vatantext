<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Admin Panel" />
<meta name="author" content="potenzaglobalsolutions.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Admin Panel</title>


<link rel="shortcut icon" href="<?php echo site_url('assets_admin/images/logo-icon-dark.png'); ?>" />
<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets_admin/css/style.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets_admin/dropzone/dropzone.css'); ?>" />
 
</head>

<body>

<div class="wrapper">

 
<div id="pre-loader">
    <img src="<?php echo site_url('assets_admin/images/pre-loader/loader-01.svg'); ?>" alt="">
</div>

<nav class="admin-header navbar navbar-default col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
	<div class="text-left navbar-brand-wrapper">
		<a href="<?php echo site_url('admin/Admin'); ?>"><h3 class="pl-4" style="color: green;">Admin Panel</h3></a>
	</div>
	  <!-- Top bar left -->
	<ul class="nav navbar-nav mr-auto">
	    <li class="nav-item">
	      	<a id="button-toggle" class="button-toggle-nav inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu ti-align-right"></i></a>
	    </li>
	</ul>
	  <!-- top bar right -->
	<ul class="nav navbar-nav ml-auto">
	    <li class="nav-item fullscreen">
	      	<a id="btnFullscreen" href="#" class="nav-link" ><i class="ti-fullscreen"></i></a>
	    </li>
	    <li class="nav-item dropdown mr-30">
	      	<a class="nav-link nav-pill user-avatar" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
	        	<img src="<?php echo site_url('assets_admin/images/logo-icon-dark.png') ?>" alt="avatar">
	      	</a>
		    <div class="dropdown-menu dropdown-menu-right">
		        <div class="dropdown-header">
		          	<div class="media">
			            <div class="media-body">
			              	<h5 class="mt-0 mb-0">Admin Panel</h5>
			            </div>
		          	</div>
		        </div>
		        <a class="dropdown-item" href="#"><i class="text-info ti-settings"></i>Site Ayarları</a>
		        <a class="dropdown-item" href="<?php echo site_url('admin/anasayfa') ?>"><i class="text-danger ti-unlock"></i>Çıkış</a>
		    </div>
	    </li>
	</ul>
</nav>

<!--=================================
 header End-->

<!--=================================
 Main content -->
 
<div class="container-fluid">
  	<div class="row">
    <!-- Left Sidebar start-->
		<div class="side-menu-fixed">
		    <div class="scrollbar side-menu-bg">
		      	<ul class="nav navbar-nav side-menu" id="sidebarnav">
		        <!-- menu item Dashboard-->

		        	<li>
		          		<a href="<?php echo site_url('admin/uye-listesi'); ?>">
		          			<div class="pull-left"><i class="ti-home"></i><span class="right-nav-text">Üye Listesi</span></div>
			            	<div class="pull-right"></div><div class="clearfix"></div>
		          		</a>
		        	</li>
			        <li>
			        <li>
				        <a href="javascript:void(0);" data-toggle="collapse" data-target="#temsilcilikler">
				            <div class="pull-left"><i class="ti-flag-alt"></i><span class="right-nav-text">Blog</span></div>
				            <div class="pull-right"><i class="ti-plus"></i></div><div class="clearfix"></div>
				        </a>
				        <ul id="temsilcilikler" class="collapse" data-parent="#sidebarnav">
				            <li> <a href="<?php echo site_url('admin/blog-ekle'); ?>">Ekle</a> </li>
				            <li> <a href="<?php echo site_url('admin/blog-listele'); ?>">Listele</a> </li>
				        </ul>
			        </li>
		    	</ul>
		  	</div> 
		</div> 


