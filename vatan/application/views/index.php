<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Webmin - Bootstrap 4 & Angular 5 Admin Dashboard Template" />
<meta name="author" content="potenzaglobalsolutions.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Admin Panel Giriş</title>


<link rel="shortcut icon" href="<?php echo site_url('assets_admin/images/logo-icon-dark.png'); ?>" />
<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets_admin/css/style.css'); ?>" />
 
</head>

	<body>

		<div class="wrapper">
			<div id="pre-loader">
			    <img src="<?php echo site_url('assets_admin/images/pre-loader/loader-01.svg'); ?>" alt="">
			</div>

			<section class="height-100vh d-flex align-items-center page-section-ptb login" >
				<div class="container">
					<div class="index-style row justify-content-center no-gutters vertical-align text-center">
						<div  class="col-lg-4 col-md-6 login-fancy-bg bg-light" style="text-align: center;">
							<h1 class="mt-5">Kayıt Ol</h1>
                            <button onclick="window.location.href='<?php echo site_url('admin'); ?>'" class="w-100 btn btn-warning buttonAnimation" data-animation="pulse">Giriş Yap<i class="la la-refresh"></i></button>

                        </div>
						<div class="col-lg-4 col-md-6 bg-white">
							<div class="login-fancy pb-40 clearfix">
								<form id="infoAddKayit" method="post">
<!--
									<input type="hidden" name="<?php //echo $csrf['name']; ?>" value="<?php // echo $csrf['hash']; ?>" />
-->
									<div class="section-field " >
										<label class="mb-10" for="name">Ad Soyad</label>
										<input id="name" class="web form-control" type="text" name="name_surname">
										
									</div>
									<div class="section-field">
										<label class="mb-10" for="name">E-posta</label>
										<input id="name" class="web form-control" type="text" name="email">
										
									</div>
									<div class="section-field ">
										<label class="mb-10" for="name">Kullanıcı Adı</label>
										<input id="name" class="web form-control" type="text" name="username">
										
									</div>
									<div class="section-field ">
										<label class="mb-10" for="Password">Şifre </label>
										<input id="Password" class="Password form-control" type="password" name="password">
									</div>
									<button class="button" >Kayıt Ol<i class="fa fa-check"></i></button>
								</form>
                                <button onclick="window.location.href='<?php echo site_url('admin/anasayfa'); ?>'" class="w-100 btn btn-warning buttonAnimation" data-animation="pulse">Giriş Yap<i class="la la-refresh"></i></button>

                            </div>
						</div>
					</div>
				</div>
			</section> 
		</div>

		<script src="<?php echo site_url('assets_admin/js/jquery-3.3.1.min.js'); ?>"></script>



		<!-- plugins-jquery -->
		<script src="<?php echo site_url('assets_admin/js/plugins-jquery.js'); ?>"></script>

        <script src="<?php echo site_url('assets_admin/js/cookie.js'); ?>"></script>

        <script src="<?php echo site_url('assets_admin/js/ajax.js'); ?>"></script>

		<!-- plugin_path-->
		<script>var plugin_path = '<?php echo site_url('assets_admin/js/'); ?>';</script>

		<!-- chart -->
		<script src="<?php echo site_url('assets_admin/js/chart-init.js'); ?>"></script>

		<!-- calendar -->
		<script src="<?php echo site_url('assets_admin/js/calendar.init.js'); ?>"></script>

		<!-- charts sparkline -->
		<script src="<?php echo site_url('assets_admin/js/sparkline.init.js'); ?>"></script>

		<!-- charts morris -->
		<script src="<?php echo site_url('assets_admin/js/morris.init.js'); ?>"></script>

		<!-- datepicker -->
		<script src="<?php echo site_url('assets_admin/js/datepicker.js'); ?>"></script>

		<!-- sweetalert2 -->
		<script src="<?php echo site_url('assets_admin/js/sweetalert2.js'); ?>"></script>

		<!-- toastr -->
		<script src="<?php echo site_url('assets_admin/js/toastr.js'); ?>"></script>



		<!-- validation -->
		<script src="<?php echo site_url('assets_admin/js/validation.js'); ?>"></script>

		<!-- lobilist -->
		<script src="<?php echo site_url('assets_admin/js/lobilist.js'); ?>"></script>
		 
		<!-- custom -->
		<script src="<?php echo site_url('assets_admin/js/custom.js'); ?>"></script>
	 
	</body>
</html>