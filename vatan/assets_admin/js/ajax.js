(function($) {
    const url = 'http://localhost/vatan/Ajax/';
    const url2 = 'http://localhost/vatan/';
    $(document).on("submit", "#infoAddKayit", function(event){

            event.preventDefault();
            var serialized = $(this).serializeArray();
            serialized.push({
                name: 'csrf',
                value: Cookies.get('csrf_token')
            });
            $.ajax({
                method: 'post',
                dataType: 'json',
                data: serialized,
                url: url + 'add_kayit',
                success: function(result) {
                    if (result.status === true) {
                        toastr.success('Kayıt Başarılı');
                        setTimeout(
                            function(){
                            window.location.reload();
                            }, 5000);
                    } else {
                        toastr.warning(result.error);
                    }
                },
                error: function() {
                    toastr.error('Bağlantı Hatası, Lütfen Tekrar Deneyin');
                }
            });
    });

    $(".delete-users").click(function(){

            var id = $(this).data("id");

            var data = [
            {
                name: 'csrf',
                value: Cookies.get('csrf_token')
            },{
                name : 'id',
                value : id
            }]

            $.ajax({
                method: 'post',
                dataType: 'json',
                data: data,
                url: url2 + 'admin/Ajax/delete_users',
                success: function(result) {
                    if (result.status === true) {
                        $('#delete'+id).remove();
                        toastr.success('Üye Silinidi');
                    } else {
                        toastr.warning(result.error);
                    }
                },
                error: function() {
                    toastr.error('Bağlantı Hatası, Lütfen Tekrar Deneyin');
                }
            });
    });

    $(document).on("submit", "#infoAddBlog", function(event){
            event.preventDefault();
            var serialized = $(this).serializeArray();
            serialized.push({
                name: 'csrf',
                value: Cookies.get('csrf_token')
            });
            $.ajax({
                method: 'post',
                dataType: 'json',
                data: serialized,
                url: url2 + 'admin/Ajax/add_blog',
                success: function(result) {
                    if (result.status === true) {
                        toastr.success('Ekleme İşlemi Başarılı');
                        setTimeout(
                            function(){
                            window.location.reload();
                            }, 5000);
                    } else {
                        toastr.warning(result.error);
                    }
                },
                error: function() {
                    toastr.warning('Bağlantı Hatası, Lütfen Tekrar Deneyin');
                }
            });
    });

    $(".blog-status").change(function(){

            var id = $(this).data("id");

            if($(this).prop('checked')) {
                var status = 1;
            } else {
                var status = 0;
            }

            var data = [
            {
                name: 'csrf',
                value: Cookies.get('csrf_token')
            },{
                name : 'status',
                value : status
            },{
                name : 'id',
                value : id
            }]
            $.ajax({
                method: 'post',
                dataType: 'json',
                data: data,
                url: url2 + 'admin/Ajax/status_blog',
                success: function(result) {
                    if (result.status === true && status == 0) {
                        toastr.success('Sitede Görünmeyecek');
                    } else if (result.status === true && status == 1) {
                        toastr.success('Sitede Görünecek');
                    } else {
                        toastr.warning(result.error);
                    }
                },
                error: function() {
                    toastr.error('Bağlantı Hatası, Lütfen Tekrar Deneyin');
                }
            });
    });

    $(document).on("submit", "#infoUpdateBlog", function(event){
            event.preventDefault();
            var serialized = $(this).serializeArray();
            serialized.push({
                name: 'csrf',
                value: Cookies.get('csrf_token')
            });
            $.ajax({
                method: 'post',
                dataType: 'json',
                data: serialized,
                url: url2 + 'admin/Ajax/update_blog',
                success: function(result) {
                    if (result.status === true) {
                        toastr.success('Güncelleme Başarılı')
                        setTimeout(
                            function(){
                            window.location.reload();
                            }, 5000);
                    } else {
                        toastr.error(result.error);
                    }
                },
                error: function() {
                    toastr.error('Bağlantı Hatası, Lütfen Tekrar Deneyin');
                }
            });
    });

    $(".delete-blog").click(function(){

            var id = $(this).data("id");

            var data = [
            {
                name: 'csrf',
                value: Cookies.get('csrf_token')
            },{
                name : 'id',
                value : id
            }]

            $.ajax({
                method: 'post',
                dataType: 'json',
                data: data,
                url: url2 + 'admin/Ajax/delete_blog',
                success: function(result) {
                    if (result.status === true) {
                        $('#delete'+id).remove();
                        toastr.success('Blog Silindi');
                    } else {
                       toastr.warning(result.error);
                    }
                },
                error: function() {
                    toastr.error('Bağlantı Hatası, Lütfen Tekrar Deneyin');
                }
            });
    });

})(jQuery);